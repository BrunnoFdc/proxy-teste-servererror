import ngrok from 'ngrok'
import startCatcher from './request-catcher.mjs'
import { createRequire } from 'module';

const require = createRequire(import.meta.url);

const { 
    ngrokAuth,
    proxyPort,
    realUrl,
    responseCode,
    whitelistedEndpoints 
} = require('../config.json')



const start = async () => {
    const ngrokUrl = await ngrok.connect({
        authtoken: ngrokAuth,
        proto: 'http',
        addr: proxyPort,
        region: 'sa',
    })

    startCatcher(realUrl, whitelistedEndpoints, ngrokUrl, proxyPort, responseCode)
}

start()