import express from 'express'
import createError from 'http-errors'
import httpProxy from 'http-proxy'

export default function(realUrl, whitelist = [], ngrokUrl, port, status = 500) {
    let actualWhite = [...whitelist]
    const app = express()

    const proxy = httpProxy.createProxyServer({
        changeOrigin: true,
        target: {
            https: true
        },
        host: 'localhost',
    });
    
    app.use(express.json())
    
    app.use((req, res, next) => {
        const { url } = req
        console.log('Request: ' + url)
        
        if(isInWhitelist(whitelist, url)) {
            console.log('Request na Whitelist! Propagando...')
    
            const propagation = realUrl + url
    
            console.log(`URL propagada: ${propagation}`)
            proxy.web(req, res, {
                target: realUrl,
            }, next);

        } else {
            next(createError(status))
        }
    
    })
    
    app.listen(port)
    
    console.log('Servidor iniciado!')
    console.log(`Use o URL '${ngrokUrl}' como substituto do serviço a ser testado!`)
}

function isInWhitelist(whitelist, url) {
    return !!whitelist.find(whitelisted => url.includes(whitelisted))
}
