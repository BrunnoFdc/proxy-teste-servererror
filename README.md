# Proxy para teste de indisponibilidade
> Aplicação proxy destinado para testes de indisponibilidade de APIs sem necessidade de mocks ou indisponibilidades reais (Derrubando serviço e afins), self-hosted (Com ngrok) e customizável.

## Configuração
A configuração do projeto pode ser customizada a partir do arquivo `config.json`, o qual dispõe das seguintes opções:

- **ngrokAuth**: Token de autorização do ngrok. Mais detalhes na [seção ngrok](#ngrok)
- **realUrl**: URL real do serviço que está sendo testado. Usado para replicar as requisições que se encaixam na whitelist.
- **proxyPort**: Porta em que irá rodar o proxy na máquina local. Padrão: `30300`. Na maioria dos casos não precisa alterar.
- **responseCode**: Código de resposta HTTP que o proxy responderá às requisições não-whitelisted.
- **whitelistedEndpoints**: Endpoints que serão redirecionados ao serviço real (Sem mock). Não é necessário colocar o path inteiro do endpoint, apenas uma parte já é o suficiente para ser considerado.

## Como usar
**Requisitos**: [Node.js v14](https://nodejs.org/)

### Passo a passo
1. Clonar o repositório

2. Abrir uma janela de terminal/cmd/powershell na pasta raiz do projeto

3. Instalar as dependências do projeto (Apenas na primeira vez):
```sh
npm install
```

4. Executar:
```sh
npm start
```

5. Após iniciado, será exibido no terminal o URL substituto. Copie-o e substitua a URL do serviço que se deseja testar a indisponibilidade pela copiada na configmap/deployment/etc da API.


## Ngrok
O serviço ngrok tem o objetivo de estabelecer um proxy reverso entre a máquina client e um serviço HTTP disponível na internet. É uma maneira simples de expor um serviço que roda na máquina local para a internet, de maneira que qualquer um consiga acessar. 

O proxy usa o ngrok para permitir que as APIs consigam se conectar, portanto, é necessário que o usuário informe um token de acesso do ngrok para que o mock funcione.

Para obtê-lo, os passos são os seguintes:

1. [Criar uma conta no serviço](https://dashboard.ngrok.com/signup)
2. Acessar a [sessão "Your Authtoken"](https://dashboard.ngrok.com/get-started/your-authtoken) do dashboard
3. Copiar o token disponibilizado.
